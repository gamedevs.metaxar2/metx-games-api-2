const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

// Define the secret key for JWT token generation
const secretKey = process.env.SECRET_KEY;

// Add JWT middleware to verify token before accessing protected routes
const verifyToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (typeof authHeader !== "undefined") {
    const token = authHeader.split(" ")[1];
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        return res.sendStatus(403).json({
          success: false,
          message: "Invalid or expired token."
        });
      }
      req.decoded = decoded;
      next();
    });
  } else {
    res.status(401).json({
      success: false,
      message: "Authorization header is missing."
    });
  }
};

const getAll = async (req, res) => {
    try {
        const snapshot = await admin.database().ref().once("value");
        const data = snapshot.val();
        return res.status(200).send(data);
      } catch (error) {
        console.error(error);
        return res.status(500).send(error);
      }
};

const getGameID = async (req, res) => {
    try {
      const { gameID } = req.params;
      const ref = admin.database().ref(`${gameID}`);
      const snapshot = await ref.once("value");
      const userData = snapshot.val();
      return res.status(200).json(userData);
    } catch (error) {
      console.error(error);
      return res.status(500).send(error);
    }
};

async function checkTransactionStatus(transactionHash, autosignerURL) {
    try {
        const response = await axios.get(`${autosignerURL}/api/v1/transactions/${transactionHash}`);
        return response.data;
    } catch (error) {
        console.error(error);
    }
}

const getUserDetails = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const ref = admin.database().ref(`${gameID}/${user}`);
    const snapshot = await ref.once("value");
    const userData = snapshot.val();
    return res.status(200).json(userData);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};



const login = async (req, res) => {
  try {
      const { gameID, user } = req.params;
      const { username, tokenID } = req.query;
      const now = moment().tz(timezone);
      const time = now.format('YYYY-MM-DD HH:mm:ss');   
      
      const ref = admin.database().ref(`${gameID}/${user}`);
      const generalRef = ref.child('a_General');
      const scoreRef = ref.child('b_Score');
      
      await generalRef.update({ a_TokenID: tokenID, b_LoginTime: time });

      if (!username || !tokenID) {
        return res.status(400).json({
          success: false,
          message: 'Username and token ID are required.'
        });
      }
      
      // Retrieve the current daily score and the last update time from the database
      const scoreSnapshot = await scoreRef.once('value');
      const lastUpdate = scoreSnapshot.child('b_DS_Updated').val();

      // Reset the daily score if it's a different day now
      if (lastUpdate && moment(lastUpdate).tz(timezone).isBefore(now, 'day')) {
        await scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time });
      }
      
      const token = jwt.sign({ username }, secretKey, { expiresIn: '24h' }); // Generate token with username
      
      const viewModel = {
        success: true,
        message: "User login info updated successfully",
        data: {
          gameID,
          user,
          tokenID,
          time
        },
        token: token // Add token to response
      };
      return res.status(200).json({viewModel});

    } catch (error) {
      console.error(error);
      return res.status(500).json({
        success: false,
        message: "Internal server error"
      });
    }
};

const dailyScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    let newScore = parseInt(req.query.score);
    const maxDailyScore = req.query.limit ? parseInt(req.query.limit) : null;

    if (!gameID || !user || isNaN(newScore)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}/b_Score`);

    // Retrieve the current daily score and total score from the database
    const scoreSnapshot = await ref.once('value');
    let dailyScore = scoreSnapshot.child('a_DailyScore').val() || 0;
    let totalScore = scoreSnapshot.child('d_TotalScore').val() || 0;

    if (maxDailyScore !== null) {
      // Check if the current daily score is already at the limit
      if (dailyScore >= maxDailyScore) {
        return res.status(400).json({
          success: false,
          message: "You have reached your score limit for today",
          data: {
            score: dailyScore,
            time
          }
        });
      }

      // If adding the new score would exceed the maximum limit,
      // adjust it so the daily score equals the limit
      if (dailyScore + newScore > maxDailyScore) {
        newScore = maxDailyScore - dailyScore; // Adjust newScore to limit the dailyScore
      }
    }

    // Add new score to the daily score and total score
    dailyScore += newScore;
    totalScore += newScore;

    await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

    const viewModel = {
      success: true,
      message: "Daily and total scores updated",
      data: {
        dailyScore,
        totalScore,
        time
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};


const tokensRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { hash } = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the current total score from the database
    const scoreSnapshot = await scoreRef.once('value');
    let totalScore = scoreSnapshot.child('d_TotalScore').val();

    // Ensure totalScore is a number, if not default it to 0
    if (typeof totalScore !== 'number') {
      totalScore = 0;
    }

    const amount = totalScore; // set amount equal to the total score

    // Ensure there's a request for tokens and a hash provided
    if (!amount || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no hash provided.",
        data: {},
      });
    }

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const existingHash = tokensReqSnapshot.val() ? tokensReqSnapshot.val().b_TxnHash : null;

    // If there's already a hash, then we can't proceed with another request until it's processed
    if (existingHash) {
      return res.status(200).json({
        success: false,
        message: "A token request is already in process. Please wait until it's finished.",
        data: {},
      });
    }

    await Promise.all([
      // Update the tokens request data
      tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time }),
      // Reset the total score to 0
      scoreRef.update({ d_TotalScore: 0, e_TS_Updated: time })
    ]);

    const viewModel = {
      success: true,
      message: "Tokens Requested updated and total score reset",
      data: {
        amount,
        hash,
        time,
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};


const checkClaimStatus = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { autosigner_url } = req.query;

    const ref = admin.database().ref(`${gameID}/${user}`);
    const tokensReqRef = ref.child('c_TokensReq');
    const tokensClaimRef = ref.child('d_TokensClaim');
    const scoreRef = ref.child('b_Score');

    // Retrieve the tokensRequested amount, tokensClaimed and totalScore from the database
    const tokensReqSnapshot = await tokensReqRef.once('value');
    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const scoreSnapshot = await scoreRef.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {},
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount, tokensClaimed and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to user and do nothing
    if (!tokensRequestedAmount || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {},
      });
    }

    // Call the autosigner API to get the transaction status
    const response = await axios.get(`${autosigner_url}/api/v1/transactions/${hash}`, {
      headers: { Authorization: `Bearer ${process.env.BEARER_TOKEN}` },
    });

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Check transaction status and perform updates accordingly
    const transaction = response.data.viewModel;
    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transaction.group === 'confirmed' && transaction.status === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      await Promise.all([
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
        scoreRef.update({ a_DailyScore: 0, b_DS_Updated: time}),
        tokensClaimRef.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
      ]);

      // Post to history
      postHistory(gameID, user, tokensClaimedThisTime);
    } else if (transaction.status === 'Fail') { 
      // Transaction failed but within 5 minutes, so we don't update tokensClaimed or totalScore, and don't reset tokensReq
      // Do nothing
    } else if (transaction.status !== 'Success' && now.diff(moment(transaction.deadline), 'minutes') > 5) { 
      // after 5 mins and txn status still not success: retrieve data from tokensreq to total score, tokensreq reset, daily no change
      totalScore += tokensRequestedAmount;

      await Promise.all([
        scoreRef.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        tokensReqRef.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);

      return res.status(200).json({
        success: false,
        message: "Transaction failed after 5 minutes. The requested tokens have been returned to your score.",
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    } else {
      const viewModel = {
        success: true,
        message: "Claim status checked and handled",
        data: {
          txnStatus: transaction.status,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      return res.status(200).json(viewModel);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

function postHistory(gameID, user, amount){
  const now = moment().tz(timezone);
  const time = now.format('YYYY-MM-DD HH:mm:ss'); 
  const ref = admin.database().ref(`${gameID}/${user}/d_TokensClaim/z_History`);
  ref.update({ [time]:amount });
}

const updateUpgradeSuccess = async (req, res) => {
  try {
    const { gameID, user } = req.params;

    const ref = admin.database().ref(`${gameID}/${user}`);
    const dailyUpgrade = ref.child('c_TokensReq');
    const monthlyUpgrade = ref.child('d_TokensClaim');
    const totalUpgrade = ref.child('b_Score');

    // Retrieve the tokensRequested amount, tokensClaimed, and totalScore from the database
    const tokensReqSnapshot = await dailyUpgrade.once('value');
    const tokensClaimSnapshot = await monthlyUpgrade.once('value');
    const scoreSnapshot = await totalUpgrade.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {},
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let tokensClaimedVal = tokensClaimSnapshot.val();
    let tokensClaimed = tokensClaimedVal ? tokensClaimedVal.g_TokensClaimed : 0;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount, tokensClaimed, and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof tokensClaimed !== 'number') tokensClaimed = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    const hash = tokensReqVal.b_TxnHash;

    // If there is no token request made or no transaction hash, return a message to the user and do nothing
    if (!tokensRequestedAmount || !hash || tokensRequestedAmount == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no transaction hash available.",
        data: {},
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Simulate success for this example (replace this with your actual logic)
    const transactionStatus = 'Success';

    let tokensClaimedThisTime = 0;  // Track tokens claimed in this request

    // Check if transaction is successful
    if (transactionStatus === 'Success') {
      tokensClaimedThisTime = tokensRequestedAmount;  // Tokens claimed is equal to the tokens requested
      tokensClaimed += tokensClaimedThisTime;  // Update total tokens claimed

      // reset daily score, reset tokens requested, update tokens claimed
      if (tokensRequestedAmount > 0) {
        await Promise.all([
          dailyUpgrade.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
          totalUpgrade.update({ a_DailyScore: 0, b_DS_Updated: time}),
          monthlyUpgrade.update({ g_TokensClaimed: tokensClaimed, h_TC_Updated: time}),
        ]);
      }

      const viewModel = {
        success: true,
        message: "Tokens Claimed updated successfully",
        data: {
          txnStatus: transactionStatus,
          time,
          totalScore,
          tokensRequestedAmount: 0,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      };
      postHistory(gameID, user, tokensClaimedThisTime, hash);

      return res.status(200).json(viewModel);

    } else {
      return res.status(400).json({
        success: false,
        message: "Tokens Claimed NOT updated.",
        data: {
          txnStatus: transactionStatus,
          time,
          totalScore,
          tokensRequestedAmount,
          tokensClaimed: tokensClaimed,
          tokensClaimedThisTime: tokensClaimedThisTime,
        },
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};


// KIV
const updateUpgradeFail = async (req, res) => {
  try {
    const { gameID, user } = req.params;

    const ref = admin.database().ref(`${gameID}/${user}`);
    const dailyUpgrade = ref.child('c_TokensReq');
    const totalUpgrade = ref.child('b_Score');

    // Retrieve the tokensRequested amount and totalScore from the database
    const tokensReqSnapshot = await dailyUpgrade.once('value');
    const scoreSnapshot = await totalUpgrade.once('value');

    const tokensReqVal = tokensReqSnapshot.val();

    if (!tokensReqVal || tokensReqVal == 0) {
      return res.status(200).json({
        success: false,
        message: "There is no data available for token request.",
        data: {},
      });
    }

    let tokensRequestedAmount = tokensReqVal.a_TokensReq;
    let totalScore = scoreSnapshot.val().d_TotalScore;

    // Ensure tokensRequestedAmount and totalScore are numbers, if not default them to 0
    if (typeof tokensRequestedAmount !== 'number') tokensRequestedAmount = 0;
    if (typeof totalScore !== 'number') totalScore = 0;

    // Add tokensRequestedAmount to the totalScore
    totalScore += tokensRequestedAmount;

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 

    // Update the totalScore and reset tokens requested
    if (tokensRequestedAmount > 0)
    {
      await Promise.all([
        totalUpgrade.update({ d_TotalScore: totalScore, e_TS_Updated: time }),
        dailyUpgrade.update({ a_TokensReq: 0, b_TxnHash: null, c_TR_Updated: null, d_TimeStamp: null }),
      ]);
    }
    
    const viewModel = {
      success: false,
      message: "Tokens Claimed NOT updated. Tokens requested added to total score.",
      data: {
        txnStatus: "Fail",
        time,
        totalScore,
        tokensRequestedAmount: 0,
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

module.exports = {
  verifyToken,
  getAll,
  getGameID,
  getUserDetails,
  login,
  dailyScore,
  tokensRequest,
  checkClaimStatus
};
