const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');
const { response } = require("express");

const updateHighScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { autosigner_url, eventID, auth } = req.query;
    let newScore = parseInt(req.query.score);

    if (!gameID || !user || isNaN(newScore)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    // Retrieve the current high score from the database (cannot be added up)
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');
    const ref_highScore = admin.database().ref(`${gameID}/${user}/g_HighScore`);
    const ref_highScoreEvent = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}/g_HighScore`);
    const highScoreSnapshot = await ref_highScore.once('value');
    const eventHighScoreSnapshot = await ref_highScoreEvent.once('value');
    let highScore = 0;

    if (eventID !== undefined && eventID !== null && eventID !== "") {
      highScore = highScoreSnapshot.child(eventID).val() || 0;
    } else {
      highScore = highScoreSnapshot.child('HighScore').val() || 0;
    }



    if (newScore > highScore){
      // Update the high score based on the condition
      if (eventID !== undefined && eventID !== null && eventID !== "") {
        await ref_highScore.update({ [eventID]: newScore });
        await ref_highScoreEvent.update({ [eventID]: newScore });
      } else {
        await ref_highScore.update({ HighScore: newScore });
      }

      if (autosigner_url !== null && autosigner_url !== undefined && autosigner_url !== "") {
        const autosignerResponse = await updateHighScoreAutosigner(autosigner_url, newScore, auth, gameID);

        const viewModel = {
          success: true,
          message: "New High Score Updated!",
          data: {
            time,
            highScore: newScore
          },
          autosigner: autosignerResponse
        };

        return res.status(200).json(viewModel);

      } else {
        const viewModel = {
          success: true,
          message: "New High Score Updated!",
          data: {
            time,
            highScore: newScore
          },
          autosigner: {
            success: false,
            message: "There's no autosigner used!"
          }
        };

        return res.status(200).json(viewModel);
      }

    } else {
      let currentScore = newScore;
      const viewModel = {
        success: false,
        message: "Current score below HighScore",
        data: {
          time,
          currentScore,
          highScore
        },
        autosigner: {
          success: false,
          message: "No update on autosigner!"
        }
      };

      return res.status(200).json(viewModel);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};

async function updateHighScoreAutosigner(autosigner_url, score, auth, gameID) {
  // Fetch bearer token from Firebase Realtime Database
  const metxSecureRef = admin.database().ref(`~ metx-secure`);
  const bearerTokenSnapshot = await metxSecureRef.child('bearerToken').once('value');
  const bearerToken = bearerTokenSnapshot.val();

  try {
    console.log('Autosigner Parameters:', {
      autosigner_url,
      bearerToken,
      score,
      auth,
      gameID
    });

    const response = await axios.put(`${autosigner_url}/api/v1/events/highscore?score=${score}&auth=${auth}&gameId=${gameID}`, {
      headers: { Authorization: `Bearer ${bearerToken}` },
    });

    if (response.status === 200) {
      console.log('Autosigner Update High Score successful');
      return {
        success: true,
        message: `Autosigner Update High Score successful ( ${response.message} )`
      }
    } else {
      console.error('Autosigner Update High Score failed:', response.status, response.data);
      return {
        success: false,
        message: `Autosigner Update High Score failed: ${response.status} ( ${response.message} )`
      }
    }
  } catch (error) {
    console.error('Autosigner Not Working!:', error.message);
    return {
      success: false,
      message: `Autosigner Not Working!: ${error.message} ( ${response.message} )`
    }
  }
}


  const getHighScore = async (req, res) => {
    try {
      const { gameID, user } = req.params;
      const { eventID } = req.query;
      const ref = admin.database().ref(`${gameID}/${user}/g_HighScore`);
      const snapshot = await ref.once("value");
      const userData = snapshot.val();

      let highScore;
      if (eventID !== undefined && eventID !== null && eventID !== "") {
        highScore = userData ? userData[eventID] || 0 : 0;
      } else {
        highScore = userData ? userData['HighScore'] || 0 : 0;
      }

      let claimRef;
      if (eventID !== undefined && eventID !== null && eventID !== "") {
        claimRef = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
      } else {
        claimRef = admin.database().ref(`${gameID}/${user}`);
      }
      const tokensClaimRef = claimRef.child('d_TokensClaim');
      const tokensClaimSnapshot = await tokensClaimRef.once('value');
      const amountTokensClaim = tokensClaimSnapshot.child('g_TokensClaimed').val();

      let availableTokensClaim = false;
      if (amountTokensClaim < highScore || amountTokensClaim == null || amountTokensClaim === undefined) {
        availableTokensClaim = true;
      }


      const result = {
        success: true,
        data: {
          highScore,
          availableTokensClaim,
          tokensClaimed: amountTokensClaim,
        },
      };

      return res.status(200).json(result);
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        success: false,
        message: "Internal server error",
      });
    }
  };


  const getHighScoreLeaderboard = async (req, res) => {
    try {
      const { gameID } = req.params;
      const { eventID } = req.query;
      const ref = admin.database().ref(`${gameID}`);

      const snapshot = await ref.once('value');
      const gameData = snapshot.val();

      if (!gameData) {
        return res.status(404).json({ error: 'Game not found' });
      }

      // Extract high scores and user IDs based on the eventID
      const leaderboard = Object.entries(gameData)
        .filter(([userID, userData]) => userData.g_HighScore)
        .map(([userID, userData]) => ({
          userID,
          highscore: eventID ? userData.g_HighScore[eventID] || 0 : userData.g_HighScore.HighScore || 0,
        }));

      // Sort leaderboard in descending order based on high scores
      const sortedLeaderboard = leaderboard.sort((a, b) => b.highscore - a.highscore);

      return res.json(sortedLeaderboard);
    } catch (error) {
      console.error(error);
      return res.status(500).json({ error: 'Internal Server Error' });
    }
  };

module.exports = {
  updateHighScore,
  getHighScore,
  getHighScoreLeaderboard
  };
