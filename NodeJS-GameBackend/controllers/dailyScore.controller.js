const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const dailyScore = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    let newScore = parseInt(req.query.score);
    const maxDailyScore = req.query.limit ? parseInt(req.query.limit) : null;

    if (!gameID || !user || isNaN(newScore)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    let ref;
    const ref_highScore = admin.database().ref(`${gameID}/${user}/g_HighScore`);
    const ref_highScoreEvent = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}/g_HighScore`);

    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}/b_Score`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}/b_Score`);
    }

    // Retrieve the current daily score and total score from the database (can be added up)
    const scoreSnapshot = await ref.once('value');

    // Retrieve the current high score from the database (cannot be added up)
    const highScoreSnapshot = await ref_highScore.once('value');

    //Condition for eventid
    let highScore = highScoreSnapshot.child('HighScore').val() || 0;
    let dailyScore = scoreSnapshot.child('a_DailyScore').val() || 0;
    let totalScore = scoreSnapshot.child('d_TotalScore').val() || 0;

    if (maxDailyScore !== null) {
      // Check if the current daily score is already at the limit
      if (dailyScore >= maxDailyScore) {
        return res.status(400).json({
          success: false,
          message: "You have reached your score limit for today",
          data: {
            eventID,
            score: dailyScore,
            time
          }
        });
      }

      // If adding the new score would exceed the maximum limit,
      // adjust it so the daily score equals the limit
      if (dailyScore + newScore > maxDailyScore) {
        newScore = maxDailyScore - dailyScore; // Adjust newScore to limit the dailyScore
      }
    }

    // Add new score to the daily score and total score
    dailyScore += newScore;
    totalScore += newScore;

    if (newScore > highScore){

      if (eventID !== undefined && eventID !== null && eventID !== "") {
        await ref_highScore.update({ [eventID]: newScore });
        await ref_highScoreEvent.update({ [eventID]: newScore });
      } else {
        await ref_highScore.update({ HighScore: newScore });
      }

      await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

      const viewModel = {
        success: true,
        message: {
          dailyScore: "Daily score updated successfully",
          highScore: "New High Score!"
        },
        data: {
          eventID,
          dailyScore,
          totalScore,
          time,
          highScore: newScore
        },
      };

      return res.status(200).json(viewModel);

    } else {

      await ref.update({ a_DailyScore: dailyScore, b_DS_Updated: time, d_TotalScore: totalScore, e_TS_Updated: time });

    const viewModel = {
      success: true,
      message: {
        dailyScore: "Daily score updated successfully",
        highScore: "Current score below HighScore :("
      },
      data: {
        eventID,
        dailyScore,
        totalScore,
        time
      },
    };

      return res.status(200).json(viewModel);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};

//TODO
const getScoreTokenLeaderboard = async (req, res) => {
  const { gameID } = req.params;
  const { eventID } = req.query;
  const ref = admin.database().ref(`${gameID}`);

  try {
    // Fetch all users under the specified gameID
    const usersSnapshot = await ref.once('value');
    const users = usersSnapshot.val();

    if (!users) {
      return res.status(404).json({
        success: false,
        message: 'No users found for the specified gameID.',
      });
    }

    // Calculate sums for each user based on the eventID
    const leaderboard = [];

    for (const userID in users) {
      const userRef = ref.child(userID);

      let tokensClaimRef;
      let tokensReqRef;
      let scoreRef;

      if (eventID !== undefined && eventID !== null && eventID !== "") {
        tokensClaimRef = userRef.child('zEvent_' + eventID).child('d_TokensClaim').child('g_TokensClaimed');
        tokensReqRef = userRef.child('zEvent_' + eventID).child('c_TokensReq').child('a_TokensReq');
        scoreRef = userRef.child('zEvent_' + eventID).child('b_Score').child('d_TotalScore');
      } else {
        tokensClaimRef = userRef.child('g_TokensClaimed');
        tokensReqRef = userRef.child('c_TokensReq');
        scoreRef = userRef.child('b_Score').child('d_TotalScore');
      }

      const tokensReqSnapshot = await tokensReqRef.once('value');
      const tokensClaimSnapshot = await tokensClaimRef.once('value');
      const scoreSnapshot = await scoreRef.once('value');

      const totalScoreVal = scoreSnapshot.val() || 0;
      const tokensReqVal = tokensReqSnapshot.val() || 0;
      const tokensClaimedVal = tokensClaimSnapshot.val() || 0;

      // Calculate the sum of the values for each user
      const userSum = totalScoreVal + tokensReqVal + tokensClaimedVal;

      // Push user details to the leaderboard array
      leaderboard.push({
        user: userID,
        overallScore: userSum,
        details: {
          totalScore: totalScoreVal,
          tokensReq: tokensReqVal,
          tokensClaim: tokensClaimedVal,
        }

      });
    }

    // Sort the leaderboard based on the userSum in descending order
    leaderboard.sort((a, b) => b.overallScore - a.overallScore);

    return res.status(200).json({
      success: true,
      eventID,
      data: leaderboard,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: 'Internal server error',
    });
  }
};



module.exports = {
  dailyScore,
  getScoreTokenLeaderboard
};
