const admin = require("firebase-admin");
require('dotenv').config();


const getUserDetails = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { toggle, eventID } = req.query;
    const ref = admin.database().ref(`${gameID}/${user}`);
    const snapshot = await ref.once("value");
    let userData;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      userData = snapshot.child('zEvent_' + eventID).val();

    } else if (toggle !== undefined && toggle !== null && toggle !== "" && toggle == 1) {
      userData = snapshot.val();
      const userDetails = {
        __General__: userData.__General__,
        a_General: userData.a_General,
        b_Score: userData.b_Score,
        c_TokensReq: userData.c_TokensReq,
        d_TokensClaim: userData.d_TokensClaim,
        g_HighScore: userData.g_HighScore,
      };

      userData = userDetails;
    } else {
      userData = snapshot.val();
    }

    return res.status(200).json(userData);
  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};


module.exports = {
  getUserDetails
};
