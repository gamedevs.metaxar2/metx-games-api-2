const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const upgradesRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { hash } = req.query;
    const { upgrades } = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}`);
    const upgradeReqRef = ref.child('f_UpgradesReq'); 

    // Ensure there's a request for tokens and a hash provided
    if (!upgrades || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no hash provided.",
        data: {},
      });
    }

    const upgradeReqSnapshot = await upgradeReqRef.once('value');
    const existingHash = upgradeReqSnapshot.val() ? upgradeReqSnapshot.val().b_TxnHash : null;

    // If there's already a hash, then we can't proceed with another request until it's processed
    if (existingHash) {
      return res.status(200).json({
        success: false,
        message: "A token request is already in process. Please wait until it's finished.",
        data: {},
      });
    }
    const amount = Number(upgrades);

    await Promise.all([
      // Update the tokens request data
      upgradeReqRef.update({ a_UpgradesReq: amount, b_TxnHash: hash, c_UR_Updated: time }),
    ]);

    const viewModel = {
      success: true,
      message: "Upgrades Requested updated",
      data: {
        amount,
        hash,
        time,
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

module.exports = {
  upgradesRequest,
};
