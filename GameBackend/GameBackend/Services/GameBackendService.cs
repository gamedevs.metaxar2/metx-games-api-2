﻿using GameBackend.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace GameBackend.Services
{
    public class GameBackendService : IGameBackendService
    {
        private readonly IMongoDatabase _database;

        public GameBackendService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Game> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Game>(collectionName);
        }

        public async Task<List<Game>> GetAsync(string collectionName)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(_ => true).ToListAsync();
        }

        public async Task<Game?> GetAsync(string collectionName, string id)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task CreateAsync(string collectionName, Game newGame)
        {
            var collection = GetCollectionByName(collectionName);
            await collection.InsertOneAsync(newGame);
        }

        public async Task UpdateAsync(string collectionName, string id, Game updatedGame)
        {
            var collection = GetCollectionByName(collectionName);
            await collection.ReplaceOneAsync(x => x.Id == id, updatedGame);
        }

        public async Task RemoveAsync(string collectionName, string id)
        {
            var collection = GetCollectionByName(collectionName);
            await collection.DeleteOneAsync(x => x.Id == id);
        }
    }
}
