namespace GameBackend.Models
{
    public class ClaimStatusResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
    public class AutosignerResponse
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public TransactionViewModel ViewModel { get; set; }
    }

    public class TransactionViewModel
    {
        public string Group { get; set; }
        public string Status { get; set; }
        public string Hash { get; set; }
        public long Deadline { get; set; }
        public long Height { get; set; }
        // Add other properties as needed based on the response from the autosigner API
    }




}


