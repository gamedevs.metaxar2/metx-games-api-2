using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;


namespace GameBackend.Controllers
{
    [ApiController]
    [Route("[controller]/{collectionName}")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("{userId}")]

        public async Task<ActionResult<Player>> GetUser(string collectionName, string userId)
        {
            var user = await _userService.GetUser(collectionName, userId);

            if (user is null)
            {
                return NotFound();
            }

            return user;
        }

        [AllowAnonymous]
        [HttpPost("{userId}")]
        public async Task<IActionResult> CreateUser(string collectionName, string userId, [FromBody] User newUser)
        {
            if (string.IsNullOrEmpty(newUser?.TokenID) || string.IsNullOrEmpty(userId))
            {
                return BadRequest(new
                {
                    success = false,
                    message = "Missing data in User object"
                });
            }
            var player = new Player
            {
                UserId = userId
            };

            // Check if the user with the given ID already exists in the database
            var existingUser = await _userService.GetUser(collectionName, userId);
            if (existingUser != null)
            {
                return Conflict(new
                {
                    success = false,
                    message = "User with the same ID already exists"
                });
            }

            newUser.GameID = collectionName;

            try
            {
                var jwtToken = await _userService.CreateNewUser(collectionName, userId, newUser);

                var viewModel = new
                {
                    success = true,
                    message = "User created successfully",
                    data = newUser,
                    token = jwtToken
                };
                return StatusCode(201, viewModel);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return StatusCode(500, new
                {
                    success = false,
                    message = "Internal server error"
                });
            }
        }

        [AllowAnonymous]
        [HttpPut("{userId}")]
        public async Task<IActionResult> LoginUser(string collectionName, string userId, [FromBody] User updatedUser)
        {
            try
            {
                var user = await _userService.GetUser(collectionName, userId);

                if (user is null)
                {
                    return NotFound(new
                    {
                        success = false,
                        message = "User not found."
                    });
                }

                var jwtToken = await _userService.LoginUser(collectionName, userId, updatedUser);

                var viewModel = new
                {
                    success = true,
                    message = "User login info updated successfully",
                    data = new
                    {
                        gameID = collectionName,
                        user = userId,
                        time = DateTime.Now
                    },
                    token = jwtToken
                };

                return StatusCode(200, viewModel);
            }
            catch (Exception ex)
            {
                // Log the exception or handle it as needed
                Console.Error.WriteLine(ex);

                return StatusCode(500, new
                {
                    success = false,
                    message = "An error occurred while updating user login info."
                });
            }
        }

    }
}
