using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace GameBackend.Controllers
{
    [ApiController]
    [Route("[controller]/{collectionName}")]
    public class ScoreController : ControllerBase
    {
        private readonly IScoreService _scoreService;

        public ScoreController(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }

        [HttpPut("{userId}")]
        public async Task<IActionResult> UpdateScore(string collectionName, string userId, [FromQuery] int score, int? limit = null)
        {
            try
            {
                var scoreUpdateResult = await _scoreService.UpdateScore(collectionName, userId, score, limit);

                if (scoreUpdateResult.ExceededLimit)
                {
                    var viewModelExceeded = new
                    {
                        success = false,
                        message = "Score update exceeds daily limit",
                        data = new
                        {
                            dailyScore = scoreUpdateResult.DailyScore,
                            totalScore = scoreUpdateResult.TotalScore,
                            time = scoreUpdateResult.Time
                        }
                    };
                    return StatusCode(400, viewModelExceeded);
                }
                else
                {
                    var viewModel = new
                    {
                        success = true,
                        message = "Daily and total scores updated",
                        data = new
                        {
                            dailyScore = scoreUpdateResult.DailyScore,
                            totalScore = scoreUpdateResult.TotalScore,
                            time = scoreUpdateResult.Time
                        }
                    };
                    return StatusCode(200, viewModel);
                }
            }
            catch (ArgumentException ex)
            {
                return StatusCode(404, new
                {
                    success = false,
                    message = ex.Message
                });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(400, new
                {
                    success = false,
                    message = ex.Message
                });
            }
            catch (Exception ex)
            {
                // Log the error
                Console.Error.WriteLine(ex);
                return StatusCode(500, new
                {
                    success = false,
                    message = "Internal server error"
                });
            }
        }

    }
}
