﻿using GameBackend.Models;
using GameBackend.Services;
using Microsoft.AspNetCore.Mvc;

namespace GameBackend.Controllers
{
    [ApiController]
    [Route("api/[controller]/{collectionName}")]
    public class GameController : ControllerBase
    {
        private readonly GameBackendService _gameService;

        public GameController(GameBackendService gameService) =>
            _gameService = gameService;

        [HttpGet]
        public async Task<IActionResult> Get(string collectionName)
        {
            var gameList = await _gameService.GetAsync(collectionName);
            return Ok(gameList);
        }

        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<Game>> Get(string collectionName, string id)
        {
            var game = await _gameService.GetAsync(collectionName, id);

            if (game is null)
            {
                return NotFound();
            }

            return game;
        }

        [HttpPost]
        public async Task<IActionResult> Post(string collectionName, Game newGame)
        {
            await _gameService.CreateAsync(collectionName, newGame);

            return CreatedAtAction(nameof(Get), new { collectionName, id = newGame.Id }, newGame);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update(string collectionName, string id, Game updatedGame)
        {
            var book = await _gameService.GetAsync(collectionName, id);

            if (book is null)
            {
                return NotFound();
            }

            updatedGame.Id = book.Id;

            await _gameService.UpdateAsync(collectionName, id, updatedGame);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string collectionName, string id)
        {
            var book = await _gameService.GetAsync(collectionName, id);

            if (book is null)
            {
                return NotFound();
            }

            await _gameService.RemoveAsync(collectionName, id);

            return NoContent();
        }
    }
}
